import EventService from '@/services/EventService.js'

export const state = () => ({
  data: []
})

export const mutations = {
  SET_DATA(state, data) {
    state.data = data
  }
}

export const actions = {
  fetchData({ commit }) {
    return EventService.getData().then((response) => {
      commit('SET_DATA', response.data)
    })
  }
}
