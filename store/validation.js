export const state = () => ({
  errorStatus: {}
})

export const mutations = {
  SET_STATUS(state, field) {
    state.errorStatus[field.name] = {
      invalid: field.invalid,
      touched: field.touched,
      error: field.error
    }
  },
  TOUCH(state, name) {
    state.errorStatus[name].touched = true
    state.errorStatus[name].error =
      state.errorStatus[name].touched && state.errorStatus[name].invalid
  },
  UPDATE_ERROR(state, payload) {
    state.errorStatus[payload.name].error = payload.status
  },
  UPDATE_VALIDITY(state, payload) {
    state.errorStatus[payload.name].invalid = payload.status
    state.errorStatus[payload.name].error =
      state.errorStatus[payload.name].touched &&
      state.errorStatus[payload.name].invalid
  }
}

export const actions = {
  updateStatus({ commit }, field) {
    commit('SET_STATUS', field)
  },
  makeTouched({ commit }, name) {
    commit('TOUCH', name)
  },
  updateErrorStatus({ commit }, payload) {
    commit('UPDATE_ERROR', payload)
  },
  updateValidStatus({ commit }, payload) {
    commit('UPDATE_VALIDITY', payload)
  }
}
