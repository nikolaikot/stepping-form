export default {
  data() {
    return {
      errors: []
    }
  },

  computed: {
    isRequired() {
      return this.settings.required
    }
  },

  methods: {
    emitData(valueToEmit) {
      this.$emit('input', valueToEmit)
    },
    handleData(data) {
      this.emitData(data)
      this.validate(data)
    },
    cleanErrors() {
      this.errors = []
    },
    addError(message) {
      this.errors.push(message)
    },
    validate(value) {
      this.cleanErrors()
      this.validateByEmpty(value)
      this.$emit('validate', this.errors)
    },
    validateByEmpty(value) {
      if (!this.isRequired) return true
      const values = Object.values(value)
      const status = values.find((value) => value.length !== 0)
      if (!status) {
        this.addError('This field is required')
      }
      return !!status
    },
    validateBeforeNextStep() {
      this.validate(this.value)
    }
  },
  created() {
    this.$root.$on('validateAllFields', this.validateBeforeNextStep)
  },

  destroyed() {
    this.$root.$off('validateAllFields', this.validateBeforeNextStep)
  }
}
